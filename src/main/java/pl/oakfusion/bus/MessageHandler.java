package pl.oakfusion.bus;

import pl.oakfusion.data.message.*;

import java.io.*;

@FunctionalInterface
public interface MessageHandler<M extends Message> {
    void handle(M message) throws IOException;
}
