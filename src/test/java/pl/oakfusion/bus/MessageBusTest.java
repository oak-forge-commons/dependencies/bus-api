package pl.oakfusion.bus;

import org.junit.jupiter.api.*;
import pl.oakfusion.data.message.*;

import java.io.*;
import java.time.*;
import java.util.*;

import static java.time.LocalDateTime.*;
import static org.assertj.core.api.Assertions.*;

class MessageBusTest {

    private static final Message MESSAGE = new Message(now()) {};

    private final MessageBus messageBus = new MessageBus() {
        @Override
        public <M extends Message> void post(M message) {
            messagesPosted.add(message);
        }
    };

    private List<Message> messagesPosted = new ArrayList<>();

    private static class DerivedMessage extends Message {
        protected DerivedMessage(LocalDateTime timestamp) {
            super(timestamp);
        }
    }

    @Test
    void should_not_handle_message_without_registered_handler() {
        // when
        int handled = messageBus.handle(MESSAGE);

        // then
        assertThat(handled).isZero();
    }

    @Test
    void should_handle_message_when_handler_registered() {
        // given
        messageBus.registerMessageHandler(Message.class, message -> {});

        // when
        int handled = messageBus.handle(MESSAGE);

        // then
        assertThat(handled).isOne();
    }

    @Test
    void should_fire_two_handlers__one_for_derived_message_class_and_one_for_base_message_class() {
        // given
        messageBus.registerMessageHandler(Message.class, message -> {});
        messageBus.registerMessageHandler(DerivedMessage.class, message -> {});

        // when
        int handled = messageBus.handle(new DerivedMessage(now()));

        // then
        assertThat(handled).isEqualTo(2);
    }

    @Test
    void should_only_fire_handler_for_base_message_class() {
        // given
        messageBus.registerMessageHandler(Message.class, message -> {});
        messageBus.registerMessageHandler(DerivedMessage.class, message -> {});

        // when
        int handled = messageBus.handle(MESSAGE);

        // then
        assertThat(handled).isOne();
    }

    @Test
    void should_not_count_handler_in_case_of_exception() {
        // given
        messageBus.registerMessageHandler(Message.class, message -> { throw new IOException("error"); });

        // when
        int handled = messageBus.handle(MESSAGE);

        // then
        assertThat(handled).isZero();
    }
}